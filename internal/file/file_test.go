package file_test

import (
	"net/url"
	"testing"

	"gitlab.com/feistel/go-redirects/internal/file"
)

func TestFileMatch(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		target   string
		expected string
		f        file.File
	}{
		{
			name:     "simple match",
			f:        simple(),
			target:   "http://example.com/foo",
			expected: "/bar",
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			u, err := url.Parse(tc.target)
			True(t, err == nil, err)
			True(t, len(tc.f.Rules()) != 0, nil)

			ok, r := tc.f.Match(u)
			True(t, ok, nil)
			True(t, r.To() == tc.expected, r.To())
		})
	}
}

func TestFileMiss(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name   string
		target string
		f      file.File
	}{
		{
			name:   "simple miss",
			f:      simple(),
			target: "http://example.com/bar",
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			u, err := url.Parse(tc.target)
			True(t, err == nil, err)

			ok, _ := tc.f.Match(u)
			True(t, !ok, nil)
		})
	}
}
