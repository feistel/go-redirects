package file_test

import (
	"net/http"

	"gitlab.com/feistel/go-redirects/internal/file"
	"gitlab.com/feistel/go-redirects/rule"
)

func simple() file.File {
	f := file.File{
		Rulez: []rule.Rule{
			*rule.New("/foo", "/bar", http.StatusMovedPermanently),
		},
	}

	return f
}
