package file_test

import "testing"

//revive:disable-next-line:flag-parameter false positive
func True(t *testing.T, result bool, arg interface{}) {
	t.Helper()

	if !result {
		t.Fatalf("failed assertion: expected true: %v", arg)
	}
}
