package file

import (
	"net/url"

	"gitlab.com/feistel/go-redirects/rule"
)

// File represents a _redirects file.
type File struct {
	Rulez []rule.Rule
}

// Rules returns the rules in this file.
func (f *File) Rules() []rule.Rule {
	return f.Rulez
}

// Match returns the first matching rule for the given url.
func (f *File) Match(u *url.URL) (bool, *rule.Rule) {
	for _, r := range f.Rulez {
		if r.Match(u) {
			return true, &r
		}
	}

	return false, nil
}
