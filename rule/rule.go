package rule

import "strings"

// Rule represents a single redirection or rewrite rule.
type Rule struct {
	from string

	fromSegments []string

	params *Params

	to string

	toSegments []string

	status int

	force bool

	fromHasPlaceholder bool

	toHasPlaceholder bool
}

// Option is an option for creating a Rule.
type Option func(*Rule)

// New returns a new Rule.
func New(from string, to string, status int, opts ...Option) *Rule {
	r := Rule{
		from:               from,
		fromSegments:       splitPath(from),
		fromHasPlaceholder: strings.ContainsRune(from, ':'),
		toHasPlaceholder:   strings.ContainsRune(to, ':'),
		to:                 to,
		toSegments:         splitPath(to),
		status:             status,
		force:              false,
		params:             nil,
	}

	for _, opt := range opts {
		opt(&r)
	}

	return &r
}

// WithForce allows to set the force flag.
func WithForce(force bool) Option {
	return func(p *Rule) {
		p.force = force
	}
}

// WithParams allows to set the query parameters.
func WithParams(params *Params) Option {
	return func(p *Rule) {
		p.params = params
	}
}

// From returns the the path you want to redirect.
func (r *Rule) From() string {
	return r.from
}

// To returns the URL or path you want to redirect to.
func (r *Rule) To() string {
	return r.to
}

// Status returns the HTTP status code you want to use in that redirect.
func (r *Rule) Status() int {
	return r.status
}

// Force returns whether to override any existing content in the path or not.
func (r *Rule) Force() bool {
	return r.force
}

// Params returns the query string parameters REQUIRED to match the redirect.
func (r *Rule) Params() *Params {
	return r.params
}

func splitPath(path string) []string {
	if path[0] == '/' {
		return strings.Split(path[1:], "/")
	}

	return strings.Split(path, "/")
}
