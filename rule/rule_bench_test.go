package rule_test

import (
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/feistel/go-redirects/rule"
)

func BenchmarkMatch(b *testing.B) {
	benchmarks := []struct {
		name  string
		input string
		r     rule.Rule
	}{
		{
			name:  "simple",
			r:     *rule.New("/foo", "/bar", http.StatusMovedPermanently),
			input: "http://example.com/foo",
		},
		{
			name:  "SPA",
			r:     *rule.New("/*", "/", http.StatusOK),
			input: "http://example.com/foo/bar/fragment",
		},
		{
			name:  "placeholder",
			r:     *rule.New("/foo/:year/:month/:day", "/bar/blog/:year-:month-day.html", http.StatusOK),
			input: "http://example.com/foo/2021/01/01",
		},
	}

	//nolint:gocritic // test function
	for _, bm := range benchmarks {
		u, err := url.Parse(bm.input)
		if err != nil {
			b.Fatalf("error: %v", err)
		}

		r := bm.r
		b.Run(bm.name, func(b *testing.B) {
			b.ReportAllocs()
			for i := 0; i < b.N; i++ {
				r.Match(u)
			}
		})
	}
}

func BenchmarkRewrite(b *testing.B) {
	benchmarks := []struct {
		name  string
		input string
		r     rule.Rule
	}{
		{
			name:  "simple",
			r:     *rule.New("/foo", "/bar", http.StatusMovedPermanently),
			input: "http://example.com/foo",
		},
		{
			name:  "SPA",
			r:     *rule.New("/*", "/", http.StatusOK),
			input: "http://example.com/foo/bar/fragment",
		},
		{
			name:  "placeholder",
			r:     *rule.New("/foo/:year/:month/:day", "/bar/blog/:year/:month/day.html", http.StatusOK),
			input: "http://example.com/foo/2021/01/01",
		},
	}

	//nolint:gocritic // test function
	for _, bm := range benchmarks {
		u, err := url.Parse(bm.input)
		if err != nil {
			b.Fatalf("error: %v", err)
		}

		r := bm.r
		b.Run(bm.name, func(b *testing.B) {
			b.ReportAllocs()
			for i := 0; i < b.N; i++ {
				_, err := r.Rewrite(u)
				if err != nil {
					b.Fatalf("unexpected error: %v", err)
				}
			}
		})
	}
}
