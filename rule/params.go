package rule

// Params represents query string parameters REQUIRED to match the redirect.
type Params map[string]string

// Has returns true if the param is present.
func (p *Params) Has(key string) bool {
	_, ok := (*p)[key]

	return ok
}

// Get returns the key value.
func (p *Params) Get(key string) string {
	return (*p)[key]
}
