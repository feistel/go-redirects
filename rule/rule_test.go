package rule_test

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/feistel/go-redirects/rule"
)

//nolint:funlen // test cases
func TestValidRules(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		from     string
		to       string
		params   rule.Params
		target   string
		expected string
	}{
		{
			name:     "simple",
			from:     "/foo",
			to:       "/bar",
			target:   "http://example.com/foo",
			expected: "http://example.com/bar",
		},
		{
			name:     "splat",
			from:     "/news/*",
			to:       "/blog/:splat",
			target:   "http://example.com/news/2004/01/10/my-story",
			expected: "http://example.com/blog/2004/01/10/my-story",
		},
		{
			name:     "placeholder",
			from:     "/news/:month/:date/:year/:slug",
			to:       "/blog/:year/:month/:date/:slug",
			target:   "http://example.com/news/02/12/2004/my-story",
			expected: "http://example.com/blog/2004/02/12/my-story",
		},
		{
			name:     "placeholder_splat",
			from:     "/news/:month/:date/:year/*",
			to:       "/blog/:year/:month/:date/:splat",
			target:   "http://example.com/news/02/12/2004/my-story",
			expected: "http://example.com/blog/2004/02/12/my-story",
		},
		{
			name: "params",
			from: "/store",
			to:   "/blog/:id",
			params: rule.Params{
				"id": ":id",
			},
			target:   "http://example.com/store?id=my-blog-post",
			expected: "http://example.com/blog/my-blog-post",
		},
	}
	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			r := rule.New(tc.from, tc.to, http.StatusMovedPermanently, rule.WithParams(&tc.params))

			u, err := url.Parse(tc.target)
			True(t, err == nil, err)
			True(t, r.Match(u), nil)

			result, err := r.Rewrite(u)
			True(t, err == nil, err)

			expected, err := url.Parse(tc.expected)
			True(t, err == nil, err)

			True(t, *result == *expected, fmt.Sprintf("%v \n %v", result, expected))
		})
	}
}

func TestUnmatchedRules(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		from     string
		to       string
		expected error
		target   string
	}{
		{
			name:     "too short",
			from:     "/:foo/1/2/3/4",
			to:       "/bar/:foo/2/3/4",
			target:   "http://example.com/foo",
			expected: rule.ErrUnsupportedURL,
		},
		{
			name:     "mismatched",
			from:     "/foo/1/2/3/4",
			to:       "/bar/1/2/3/4",
			target:   "http://example.com/notfoo/1/2/3/4",
			expected: rule.ErrUnsupportedURL,
		},
		{
			name:     "mismatched_placeholder",
			from:     "/:foo/1/2/3/4",
			to:       "/:foo/1/2/3/4",
			target:   "http://example.com/notfoo/1/2/3/5",
			expected: rule.ErrUnsupportedURL,
		},
	}
	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			r := rule.New(tc.from, tc.to, http.StatusMovedPermanently)

			u, err := url.Parse(tc.target)
			True(t, err == nil, err)
			True(t, !r.Match(u), nil)

			_, err = r.Rewrite(u)
			True(t, errors.Is(err, tc.expected), err)
		})
	}
}

func TestInvalidRules(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		from     string
		to       string
		expected error
		target   string
	}{
		{
			name:     "missing placeholder",
			from:     "/foo/1/2/3/4",
			to:       "/:placeholder/1/2/3/4",
			target:   "http://example.com/foo/1/2/3/4",
			expected: rule.ErrPlaceholderNotFound,
		},
	}
	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			r := rule.New(tc.from, tc.to, http.StatusMovedPermanently)

			u, err := url.Parse(tc.target)
			True(t, err == nil, err)
			True(t, r.Match(u), nil)

			_, err = r.Rewrite(u)
			True(t, errors.Is(err, tc.expected), err)
		})
	}
}

func TestCreateRule(t *testing.T) {
	t.Parallel()

	from := "/foo"
	to := "/bar"
	status := http.StatusForbidden
	force := true
	params := make(rule.Params)
	params["key"] = "value"

	r := rule.New(from, to, status,
		rule.WithForce(force),
		rule.WithParams(&params),
	)

	True(t, r.From() == from, r.From())
	True(t, r.To() == to, r.To())
	True(t, r.Status() == status, r.Status())
	True(t, r.Force() == force, r.Force())
	True(t, r.Params() == &params, r.Params())
	True(t, r.Params().Has("key"), nil)
	True(t, r.Params().Get("key") == "value", r.Params().Get("key"))
}
