package rule

import (
	"errors"
	"fmt"
	"net/url"
	"strings"
)

var (
	// ErrPlaceholderNotFound is returned if the rule contains an unknown placeholder.
	ErrPlaceholderNotFound = errors.New("placeholder not found")

	// ErrUnsupportedURL is returned when rewriting a non-matching url.
	ErrUnsupportedURL = errors.New("unsupported url")
)

// Rewrite applies the Rule to the url u
//
// Trying to apply the Rule to a non-matching url
// is invalid and return an error.
func (r *Rule) Rewrite(u *url.URL) (*url.URL, error) {
	if !r.toHasPlaceholder {
		return r.rewriteSimple(u)
	}

	path := u.Path
	pathSegments := strings.Split(path[1:], "/")
	fromRuleSegments := r.fromSegments

	if len(pathSegments) < len(fromRuleSegments) {
		return nil, fmt.Errorf("segment length too short: %w", ErrUnsupportedURL)
	}

	m, err := r.computePlaceholders(u, pathSegments)
	if err != nil {
		return nil, err
	}

	var builder strings.Builder

	for _, segment := range r.toSegments {
		builder.WriteRune('/')

		if !strings.HasPrefix(segment, ":") {
			builder.WriteString(segment)

			continue
		}

		if s, ok := m[segment]; ok {
			builder.WriteString(s)

			continue
		}

		return nil, fmt.Errorf("error replacing %s: %w", segment, ErrPlaceholderNotFound)
	}

	result := *u
	result.Path = builder.String()
	result.RawQuery = ""

	return &result, nil
}

func (r *Rule) rewriteSimple(u *url.URL) (*url.URL, error) {
	if !r.Match(u) {
		return nil, ErrUnsupportedURL
	}

	result := *u
	result.Path = r.to
	result.RawQuery = ""

	return &result, nil
}

// computePlaceholders returns the map of placeholders.
func (r *Rule) computePlaceholders(u *url.URL, pathSegments []string) (map[string]string, error) {
	m := make(map[string]string)

	for i, segment := range r.fromSegments {
		if strings.HasPrefix(segment, ":") {
			m[segment] = pathSegments[i]

			continue
		}

		if segment == "*" {
			m[":splat"] = strings.Join(pathSegments[i:], "/")

			break
		}

		if segment != pathSegments[i] {
			return nil, fmt.Errorf("mismatched segment: expected %s, got %s: %w", segment, pathSegments[i], ErrUnsupportedURL)
		}
	}

	if r.params != nil {
		for k, v := range *r.params {
			if u.Query().Get(k) != "" {
				m[v] = u.Query().Get(k)
			}
		}
	}

	return m, nil
}

// Match reports whether the url u match the Rule r.
func (r *Rule) Match(u *url.URL) bool {
	path := u.Path

	if !r.fromHasPlaceholder {
		from := r.From()
		if from[len(from)-1] == '*' {
			return strings.HasPrefix(path, from[:len(from)-1])
		}

		return from == path
	}

	pathSegments := strings.Split(path[1:], "/")

	if len(pathSegments) < len(r.fromSegments) {
		return false
	}

	for i, segment := range r.fromSegments {
		if segment == "*" {
			return true
		}

		if strings.HasPrefix(segment, ":") {
			continue
		}

		if segment != pathSegments[i] {
			return false
		}
	}

	return true
}
