package parse

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"

	"gitlab.com/feistel/go-redirects/file"
	internalFile "gitlab.com/feistel/go-redirects/internal/file"
	"gitlab.com/feistel/go-redirects/rule"
)

var (
	// ErrFileTooLarge is returned if the number of rules is above the maximum.
	ErrFileTooLarge = errors.New("file too large")

	// ErrInvalidStatusCode is returned if a status code is not allowed.
	ErrInvalidStatusCode = errors.New("invalid status code")

	// ErrInvalidParameter is returned if a query parameter is invalid.
	ErrInvalidParameter = errors.New("invalid parameter")

	// ErrMalformedRule is returned if the rule is malformed.
	ErrMalformedRule = errors.New("malformed rule")

	// ErrTooManyPathSegments is returned if the rule contains too many path segments.
	ErrTooManyPathSegments = errors.New("too many path segments")
)

// ParseFile returns a File.
func (p *Parser) ParseFile(r io.Reader) (file.File, error) {
	s := bufio.NewScanner(r)

	var rules []rule.Rule

	i := 0
	for s.Scan() {
		i++

		line := strings.TrimSpace(s.Text())

		if line == "" {
			continue
		}

		if strings.HasPrefix(line, "#") {
			continue
		}

		r, err := p.parseRule(line)
		if err != nil {
			return nil, err
		}

		for _, v := range p.validators {
			if err := v(&r); err != nil {
				return nil, err
			}
		}

		rules = append(rules, r)
	}

	if err := s.Err(); err != nil {
		return nil, fmt.Errorf("error while parsing the file: %w", err)
	}

	if len(rules) > p.maxRules {
		return nil, ErrFileTooLarge
	}

	f := internalFile.File{
		Rulez: rules,
	}

	return &f, nil
}

func (p *Parser) parseRule(line string) (rule.Rule, error) {
	fields := strings.Fields(line)
	l := len(fields)
	min := 2

	if l < min {
		return rule.Rule{}, ErrMalformedRule
	}

	if l == min {
		r := rule.New(fields[0], fields[1], p.defaultStatus)

		return *r, nil
	}

	from := fields[0]
	q := fields[1 : l-2]
	to := fields[l-2]
	code := fields[l-1]

	if len(strings.Split(from, "/")) > p.maxPathSegments {
		return rule.Rule{}, ErrTooManyPathSegments
	}

	status, force, err := p.parseStatus(code)
	if err != nil {
		if len(q) == 0 {
			// status is required, return an error
			return rule.Rule{}, err
		}

		status = p.defaultStatus
		to = code
		q = fields[1 : l-1]
	}

	opts := []rule.Option{
		rule.WithForce(force),
	}

	if len(q) > 0 {
		params, err := p.parseParams(q)
		if err != nil {
			return rule.Rule{}, err
		}

		opts = append(opts, rule.WithParams(params))
	}

	r := rule.New(from, to, status, opts...)

	return *r, nil
}

func (*Parser) parseStatus(s string) (code int, force bool, err error) {
	if strings.HasSuffix(s, "!") {
		force = true
		s = s[:len(s)-1]
	}

	//nolint:gomnd // base
	c, err := strconv.ParseInt(s, 10, 0)
	if err != nil {
		return 0, false, fmt.Errorf("code %s is invalid: %w", s, ErrInvalidStatusCode)
	}

	code = int(c)

	return code, force, nil
}

func (*Parser) parseParams(fields []string) (*rule.Params, error) {
	m := make(rule.Params)

	for _, f := range fields {
		parts := strings.Split(f, "=")

		//nolint:gomnd // params should be key=value
		if len(parts) != 2 {
			return nil, ErrInvalidParameter
		}

		m[parts[0]] = parts[1]
	}

	return &m, nil
}
