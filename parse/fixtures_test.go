package parse_test

const (
	InvalidRule   = "/home"
	InvalidStatus = "/home              /                foo"
	InvalidParams = "/articles id=:id tag=:tag foo=bar=param /posts/:tag/:id 301"

	Small           = "/home              /"
	SmallWithStatus = "/home              /                302"
	SmallWithForce  = "/home              /                302!"

	SmallWithParams       = "/articles id=:id tag=:tag /posts/:tag/:id"
	SmallWithParamsStatus = "/articles id=:id tag=:tag /posts/:tag/:id 301"

	Big = `
/home         /              301

/foo/bar      /              403

/my-redirect  /              302

/ecommerce    /store-closed  404

/pass-through /index.html    200
`

	BigWithComment = `
# Redirect with a 301
/home         /              301

# Redirect with a 403
/foo/bar      /              403

# Redirect with a 302
/my-redirect  /              302

# Show a custom 404 for this path
/ecommerce    /store-closed  404

# Rewrite a path
/pass-through /index.html    200
`

	// https://docs.netlify.com/routing/redirects/#syntax-for-the-redirects-file
	Example1 = `
# Redirects from what the browser requests to what we serve
/home              /
/blog/my-post.php  /blog/my-post
/news              /blog
/cuties            https://www.petsofnetlify.com
`

	// https://docs.netlify.com/routing/redirects/#rule-processing-order
	Example2 = `
# This rule will trigger at /blog/my-old-title
/blog/my-old-title   /blog/my-new-title

# This rule will never trigger because the previous rule triggers first
/blog/my-old-title   /blog/an-even-better-title
`

	// https://docs.netlify.com/routing/redirects/redirect-options/#splats
	Example3 = `
# This will redirect /jobs/customer-ninja-rockstar
/jobs/customer-ninja-rockstar  /careers/support-engineer

# This will redirect all paths under /jobs except the path above
/jobs/*                        /careers/:splat

# This will never trigger, because the rule above will trigger first
/jobs/outdated-job-link        /careers/position-filled
`

	// https://docs.netlify.com/routing/redirects/redirect-options/#placeholders
	Example4 = "/news/:month/:date/:year/:slug  /blog/:year/:month/:date/:slug"

	// https://docs.netlify.com/routing/redirects/redirect-options/#query-parameters
	Example5 = `
# Both values - ordering from the browser doesn’t matter; this will cover either.
/path/* param1=:value1 param2=:value2 /otherpath/:value1/:value2/:splat 301

# One value or the other.  Must match exactly.
/path/* param1=:value1 /otherpath/:value1/:splat 301
/path/* param2=:value2 /otherpath/:value2/:splat 301

# Base case, when no params are passed.  
# Our system forwards all query params to the /otherpath URL if this is the only rule.  
# Otherwise it matches all requests not matched in a prior rule for the same path such as the ones above.
/path/* /otherpath/:splat 301
`
	// https://docs.netlify.com/routing/redirects/redirect-options/#trailing-slash
	Example6 = `
# These rules are effectively the same:
# either rule alone would trigger on both paths
/blog/title-with-a-typo    /blog/typo-free-title
/blog/title-with-a-typo/   /blog/typo-free-title

# This rule will cause an infinite redirect
# because the paths are effectively the same
/blog/remove-my-slashes/   /blog/remove-my-slashes  301!
`
)
