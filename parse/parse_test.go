package parse_test

import (
	"errors"
	"fmt"
	"net/http"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/feistel/go-redirects/parse"
	"gitlab.com/feistel/go-redirects/rule"
)

//nolint:funlen // test cases
func TestValidFile(t *testing.T) {
	t.Parallel()

	defaultStatus := http.StatusTeapot
	p := parse.New(
		parse.WithDefaultStatusCode(defaultStatus),
	)

	testCases := []struct {
		name          string
		s             string
		expectedRules []rule.Rule
	}{
		{
			name: "small",
			s:    Small,
			expectedRules: []rule.Rule{
				*rule.New("/home", "/", defaultStatus),
			},
		},
		{
			name: "small_status",
			s:    SmallWithStatus,
			expectedRules: []rule.Rule{
				*rule.New("/home", "/", http.StatusFound),
			},
		},
		{
			name: "small_force",
			s:    SmallWithForce,
			expectedRules: []rule.Rule{
				*rule.New("/home", "/", http.StatusFound, rule.WithForce(true)),
			},
		},
		{
			name: "small_params",
			s:    SmallWithParams,
			expectedRules: []rule.Rule{
				*rule.New("/articles", "/posts/:tag/:id", defaultStatus, rule.WithParams(&rule.Params{"id": ":id", "tag": ":tag"})),
			},
		},
		{
			name: "small_params_status",
			s:    SmallWithParamsStatus,
			expectedRules: []rule.Rule{
				*rule.New("/articles", "/posts/:tag/:id", http.StatusMovedPermanently,
					rule.WithParams(&rule.Params{"id": ":id", "tag": ":tag"}),
				),
			},
		},
		{
			name: "big",
			s:    Big,
			expectedRules: []rule.Rule{
				*rule.New("/home", "/", http.StatusMovedPermanently),
				*rule.New("/foo/bar", "/", http.StatusForbidden),
				*rule.New("/my-redirect", "/", http.StatusFound),
				*rule.New("/ecommerce", "/store-closed", http.StatusNotFound),
				*rule.New("/pass-through", "/index.html", http.StatusOK),
			},
		},
		{
			name: "big_comment",
			s:    BigWithComment,
			expectedRules: []rule.Rule{
				*rule.New("/home", "/", http.StatusMovedPermanently),
				*rule.New("/foo/bar", "/", http.StatusForbidden),
				*rule.New("/my-redirect", "/", http.StatusFound),
				*rule.New("/ecommerce", "/store-closed", http.StatusNotFound),
				*rule.New("/pass-through", "/index.html", http.StatusOK),
			},
		},
		{
			name: "example1",
			s:    Example1,
			expectedRules: []rule.Rule{
				*rule.New("/home", "/", defaultStatus),
				*rule.New("/blog/my-post.php", "/blog/my-post", defaultStatus),
				*rule.New("/news", "/blog", defaultStatus),
				*rule.New("/cuties", "https://www.petsofnetlify.com", defaultStatus),
			},
		},
		{
			name: "example2",
			s:    Example2,
			expectedRules: []rule.Rule{
				*rule.New("/blog/my-old-title", "/blog/my-new-title", defaultStatus),
				*rule.New("/blog/my-old-title", "/blog/an-even-better-title", defaultStatus),
			},
		},
		{
			name: "example3",
			s:    Example3,
			expectedRules: []rule.Rule{
				*rule.New("/jobs/customer-ninja-rockstar", "/careers/support-engineer", defaultStatus),
				*rule.New("/jobs/*", "/careers/:splat", defaultStatus),
				*rule.New("/jobs/outdated-job-link", "/careers/position-filled", defaultStatus),
			},
		},
		{
			name: "example4",
			s:    Example4,
			expectedRules: []rule.Rule{
				*rule.New("/news/:month/:date/:year/:slug", "/blog/:year/:month/:date/:slug", defaultStatus),
			},
		},
		{
			name: "example5",
			s:    Example5,
			expectedRules: []rule.Rule{
				*rule.New("/path/*", "/otherpath/:value1/:value2/:splat", http.StatusMovedPermanently,
					rule.WithParams(&rule.Params{"param1": ":value1", "param2": ":value2"}),
				),
				*rule.New("/path/*", "/otherpath/:value1/:splat", http.StatusMovedPermanently,
					rule.WithParams(&rule.Params{"param1": ":value1"}),
				),
				*rule.New("/path/*", "/otherpath/:value2/:splat", http.StatusMovedPermanently,
					rule.WithParams(&rule.Params{"param2": ":value2"}),
				),
				*rule.New("/path/*", "/otherpath/:splat", http.StatusMovedPermanently),
			},
		},
		{
			name: "example6",
			s:    Example6,
			expectedRules: []rule.Rule{
				*rule.New("/blog/title-with-a-typo", "/blog/typo-free-title", defaultStatus),
				*rule.New("/blog/title-with-a-typo/", "/blog/typo-free-title", defaultStatus),
				*rule.New("/blog/remove-my-slashes/", "/blog/remove-my-slashes", http.StatusMovedPermanently, rule.WithForce(true)),
			},
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			reader := strings.NewReader(tc.s)
			f, err := p.ParseFile(reader)
			True(t, err == nil, err)
			True(t, reflect.DeepEqual(f.Rules(), tc.expectedRules), fmt.Sprintf("\n %v \n", f.Rules()))
		})
	}
}

func TestInvalidFile(t *testing.T) {
	t.Parallel()

	p := parse.New(
		parse.WithDefaultStatusCode(http.StatusMovedPermanently),
	)

	testCases := []struct {
		name        string
		expectedErr error
		f           string
	}{
		{
			name:        "rule",
			f:           InvalidRule,
			expectedErr: parse.ErrMalformedRule,
		},
		{
			name:        "status",
			f:           InvalidStatus,
			expectedErr: parse.ErrInvalidStatusCode,
		},
		{
			name:        "params",
			f:           InvalidParams,
			expectedErr: parse.ErrInvalidParameter,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			reader := strings.NewReader(tc.f)
			_, err := p.ParseFile(reader)
			True(t, errors.Is(err, tc.expectedErr), err)
		})
	}
}
