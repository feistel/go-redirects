package parse

import (
	"net/http"

	"gitlab.com/feistel/go-redirects/rule"
)

const (
	defaultMaxPathSegments = 25

	defaultMaxRules = 1000
)

// Parser is a general parser of a _redirect file.
type Parser struct {
	validators      []Validator
	defaultStatus   int
	maxRules        int
	maxPathSegments int
}

// Option is an option for creating a Parser.
type Option func(*Parser)

// Validator is a function for validating a Rule.
type Validator func(*rule.Rule) error

// New returns a new Parser.
func New(opts ...Option) *Parser {
	p := Parser{
		defaultStatus:   http.StatusMovedPermanently,
		maxRules:        defaultMaxRules,
		maxPathSegments: defaultMaxPathSegments,
		validators:      nil,
	}

	for _, opt := range opts {
		opt(&p)
	}

	return &p
}

// WithDefaultStatusCode is used to set default status code of a rule.
func WithDefaultStatusCode(status int) Option {
	return func(p *Parser) {
		p.defaultStatus = status
	}
}

// WithMaxRules is used to limit the total number of rules.
func WithMaxRules(max int) Option {
	return func(p *Parser) {
		p.maxRules = max
	}
}

// WithMaxPathSegments is used to limit the number of path
// segments in rules URLs.
func WithMaxPathSegments(max int) Option {
	return func(p *Parser) {
		p.maxPathSegments = max
	}
}

// WithValidator is used to validate a Rule.
func WithValidator(v Validator) Option {
	return func(p *Parser) {
		p.validators = append(p.validators, v)
	}
}
