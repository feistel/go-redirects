package parse_test

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"testing"
	"testing/iotest"

	"gitlab.com/feistel/go-redirects/parse"
	"gitlab.com/feistel/go-redirects/rule"
)

var errValidator = errors.New("validator error")

func TestDefaultStatus(t *testing.T) {
	t.Parallel()

	p := parse.New(parse.WithDefaultStatusCode(http.StatusMovedPermanently))

	reader := strings.NewReader(Small)
	f, err := p.ParseFile(reader)
	True(t, err == nil, err)

	r := f.Rules()
	True(t, len(r) == 1, len(r))
	True(t, r[0].Status() == http.StatusMovedPermanently, r[0])
}

func TestMaxRules(t *testing.T) {
	t.Parallel()

	p := parse.New(
		parse.WithMaxRules(1),
	)

	reader := strings.NewReader(Big)
	_, err := p.ParseFile(reader)
	True(t, errors.Is(err, parse.ErrFileTooLarge), err)
}

func TestMaxPathSegments(t *testing.T) {
	t.Parallel()

	p := parse.New(
		parse.WithMaxPathSegments(1),
	)

	reader := strings.NewReader(Big)
	_, err := p.ParseFile(reader)
	True(t, errors.Is(err, parse.ErrTooManyPathSegments), err)
}

func TestValidator(t *testing.T) {
	t.Parallel()

	p := parse.New(
		parse.WithValidator(func(r *rule.Rule) error {
			return fmt.Errorf("validator error: %w", errValidator)
		}),
	)

	reader := strings.NewReader(Big)
	_, err := p.ParseFile(reader)
	True(t, errors.Is(err, errValidator), err)
}

func TestErrReader(t *testing.T) {
	t.Parallel()

	p := parse.New()

	reader := iotest.ErrReader(iotest.ErrTimeout)
	_, err := p.ParseFile(reader)
	True(t, errors.Is(err, iotest.ErrTimeout), err)
}
