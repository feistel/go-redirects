package parse_test

import (
	"net/http"
	"strings"
	"testing"

	"gitlab.com/feistel/go-redirects/parse"
)

func BenchmarkParse(b *testing.B) {
	benchmarks := []struct {
		name  string
		input string
	}{
		{
			name:  "simple",
			input: "/foo /bar 200",
		},
		{
			name:  "SPA",
			input: "/* / 200",
		},
		{
			name:  "placeholder",
			input: "/foo/:year/:month/:day /bar/blog/:year-:month-day.html 200",
		},
	}

	p := parse.New(parse.WithDefaultStatusCode(http.StatusMovedPermanently))

	for _, bm := range benchmarks {
		reader := strings.NewReader(bm.input)
		b.Run(bm.name, func(b *testing.B) {
			b.ReportAllocs()
			for i := 0; i < b.N; i++ {
				_, err := p.ParseFile(reader)
				if err != nil {
					b.Fatalf("unexpected error: %v", err)
				}
			}
		})
	}
}
