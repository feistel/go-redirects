package file

import (
	"net/url"

	"gitlab.com/feistel/go-redirects/rule"
)

// File represents a _redirects file.
type File interface {
	Rules() []rule.Rule
	Match(*url.URL) (bool, *rule.Rule)
}
